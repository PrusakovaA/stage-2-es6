import { createElement } from "../../helpers/domHelper";

export function showWinnerModal(fighter) {
  const winner = `${fighter.name} is winner!!!`;
  const body = createFighterImage(fighter);
  
  const show = {
    title: winner,
    bodyElement: body,
    onClose: () => {
      window.location.reload();
    }
  };
showModal(show);
}
