import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {

  return new Promise((resolve) => {
    const firstPlayer = {
      ...firstFighter,
      isBlocked: false,
      health: firstFighter.health,
      bar: document.getElementById('left-fighter-indicator')
    };
  
    const secondPlayer = {
      ...secondFighter,
      isBlocked: false,
      health: secondFighter.health,
      bar: document.getElementById('right-fighter-indicator')
    };
  
    document.addEventListener('keydown', (event) =>{
     const pressedKey = event.code;
      if (pressedKey === controls.PlayerOneBlock) {
        firstPlayer.isBlocked = true;
      }
      if (pressedKey === controls.PlayerTwoBlock) {
        secondPlayer.isBlocked = true;
      }
      if (pressedKey === controls.PlayerOneAttack) {
        hit(firstPlayer, secondPlayer);
      } else if (pressedKey == controls.PlayerTwoAttack) {
        hit(secondPlayer, firstPlayer);
      }
    });
  
    document.addEventListener('keyup', (event) => {
     const pressedKey = event.code;
      if (pressedKey === controls.PlayerOneBlock) {
        firstPlayer.isBlocked = false;
      } else if (pressedKey === controls.PlayerTwoBlock) {
        secondPlayer.isBlocked = false;
      }
    });
    
    if (firstPlayer.health <= 0) {
      resolve(secondFighter);
    } else if (secondPlayer.health <= 0) {
      resolve(firstFighter);
    }
  });
}

export function hit(attacker, defender) {
  if (attacker.isBlocked) {
   let damage = getDamage(attacker, defender);
   defender.health -= damage;
   defender.bar.style.width = (defender.health * 100) / defender.health;
  }  
}

export function getDamage(attacker, defender) {
  let damage = getHitPower(attacker) - getBlockPower(defender);
  return damage < 0 ? 0 : damage;
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.random() + 1;
  return fighter.attack * criticalHitChance;

}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() + 1;
  return fighter.defense * dodgeChance;
}
